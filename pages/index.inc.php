<?php


	// databases
	$db_ebfw_entry		= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_ebfw_entry';
	$db_ebfw_themes 	= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_ebfw_themes';
	$db_ebfw_category 	= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_ebfw_category';
	$db_ebfw_keywords 	= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_ebfw_keywords';
	$db_ebfw_section 	= $REX['TABLE_PREFIX'].$REX['ADDON']['rxid'][$myAddon].'_ebfw_section';

	
	// addon vars
	$myAddon = 'ebfw';


	// vars
	$rowsPerPage = 40;
	$subStart = 'entry';
	$subNow = rex_request('subpage', 'string', $subStart);
	$page 	= rex_request('page', 'string');
	$func 	= rex_request('func', 'string');
	$start 	= rex_request('start', 'int');
	$id 	= rex_request('id', 'int');
	$toggle	= rex_request('toggle', 'int');


	// layout top
	include $REX['INCLUDE_PATH'].'/layout/top.php';


	// title
	$title_info = '<span style="font-size:14px; color:silver;">'.$REX['ADDON']['version'][$myAddon].'</span>';
	rex_title($REX['ADDON']['name'][$myAddon].' '.$title_info, $REX['ADDON'][$myAddon]['SUBPAGES']);


	// subpages
	if ($subNow == '' OR !in_array_multi($subNow, $REX['ADDON'][$myAddon]['SUBPAGES'])) {
		$subNow = $subStart;
	}
	$local_path = '/addons/' . $myAddon . '/pages/';
	require $REX['INCLUDE_PATH'] . $local_path . $subNow . '.inc.php';


	// layout bottom
	include $REX['INCLUDE_PATH'].'/layout/bottom.php';


?>
