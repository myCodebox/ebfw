<?php


	// vars
	$tbl = $db_ebfw_entry;
	
	
	// set status
	if( $func == 'status' ) {
		if ($id != 0) {
			echo change_status($tbl, $id, $toggle);
		} $func = '';
	}
	
	
	// set delete
	if( $func == 'delete' ) {
		if ($id != 0) {
			echo $id.' to delete';
		}
		$func = '';
	}
	

	// list output
	if ($func == '')
	{
		

		// add headline
		echo '<div class="rex-addon-output ebfw-headline">';
		echo '<h2 class="rex-hl2">'.$I18N->msg('ebfw_overview_entry').'</h2>';
		echo '</div>';
		
		
		// id, name, status, createdate, updatedate
		$query = 'SELECT id, date, name, status  FROM '.$tbl.' ORDER BY name ASC';
		$rowsPerPage = 40;
		$listName = $I18N->msg('ebfw_entry');
		$debug = FALSE;
		$list = new rex_list( $query, $rowsPerPage, $listName, $debug );
		
		
		// column width
		$list->addTableColumnGroup(array(
			array('width' => 40),
			array('width' => 115),
			array('width' => 'auto'),
			array('width' => 70, 'span' => 2)
		));


		// add new column
		$imgHeader = '<a href="'. $list->getUrl(array('func' => 'add')) .'" class="rex-i-element rex-i-metainfo-add"></a>';
		$list->addColumn($imgHeader, '<span class="rex-i-element rex-i-metainfo"></span>', 0, array( '<th class="rex-icon">###VALUE###</th>', '<td class="rex-icon">###VALUE###</td>' ) );
		$list->setColumnParams ( $imgHeader, array('func' => 'edit', 'id' => '###id###') );


		// remove column
		$list->removeColumn('id');
		
		
		// sortable
		$list->setColumnSortable('name');
		$list->setColumnSortable('date');


		// colomn label
		$list->setColumnLabel('name', $I18N->msg('ebfw_db_name'));
		$list->setColumnLabel('date', $I18N->msg('ebfw_db_date'));
		$list->setColumnLabel('status', $I18N->msg('ebfw_db_status'));
		
		
		// add date
		$list->setColumnFormat('date', 'date', 'd.m.Y / H:i');


		// add status function
		$list->setColumnParams('status', array('id' => '###id###', 'start' => $start));
		$list->setColumnFormat('status', 'custom',
			create_function(
				'$params', '$list = $params["list"];
				if($list->getValue("status") == "1") {
					$params = array("func" => "status", "id" => "###id###", "toggle" => "0");
				} else {
					$params = array("func" => "status", "id" => "###id###", "toggle" => "1");
				}
				return $list->getColumnLink("status", $list->getValue("status") != "1" ? "<span style=\'color: red;\'>Offline</span>" : "<span style=\'color: green;\'>Online</span>", $params);'
			)
		);


		// add delete
		$name = ( $list->getRows() > 0 ) ? $list->getValue('name') : $I18N->msg('ebfw_db_no_name');
		$list->addColumn('delete',$I18N->msg('delete'),-1,array('<th>'.$I18N->msg('ebfw_db_function').'</th>','<td>###VALUE###</td>'));
		$list->setColumnParams('delete', array('func' => 'delete', 'id' => '###id###'));
		$list->addLinkAttribute('delete','onclick',"return confirm('".$I18N->msg('awkl_delete_confirm', $name)."');");


		// list show
		$list->show();
		

	}

	// form output
	elseif ($func == 'edit' || $func == 'add')
	{

		
		// id, name, status, createdate, updatedate
		$tableName = $tbl;
		$fieldset = $I18N->msg('ebfw_edit_entry');
		$whereCondition = 'id='.$id;
		$method = 'post';
		$debug = FALSE;
        $form = new custom_rex_form( $tableName, $fieldset, $whereCondition, $method, $debug, 'custom_rex_form' );
		
		
		// add name
		$field = &$form->addTextField('name');
			$field->setLabel($I18N->msg('ebfw_db_name'));
			$form->addValidationMethod('name', 'empty');
			#$form->addValidationMethod('name', 'unique');
			
			
		// add date
		$field = &$form->addTextField('date');
			$field->setLabel($I18N->msg('ebfw_db_date'));
			$field->setAttribute('maxlength', 11);
			$field->setAttribute('size', 11);
			$field->setAttribute('id', 'datetimepicker');			
			$form->addValidationMethod('date', 'number');
			
		
		// add themes
		$themesSQL = 'SELECT name, id FROM '.$db_ebfw_themes.' WHERE status = 1 ORDER BY name';
		$field = &$form->addSelectField('themes');
			$field->setLabel($I18N->msg('ebfw_db_theme'));
				$select = &$field->getSelect();
				$select->setSize(1);
				$select->addOption(' - - - - - ',0);
				$select->addSqlOptions($themesSQL);
				$select->setAttribute('style','width: 460px');
				if ($field->getValue()== '') {
					$field->setValue(0);
				}
				$form->addValidationMethod('themes', 'sql_select_notnull');
				
		
		// add category
		$categorySQL = 'SELECT name, id FROM '.$db_ebfw_category.' WHERE status = 1 ORDER BY name';
		$field = &$form->addSelectField('category');
			$field->setLabel($I18N->msg('ebfw_db_category'));
				$select = &$field->getSelect();
				$select->setSize(1);
				$select->addOption(' - - - - - ',0);
				$select->addSqlOptions($categorySQL);
				$select->setAttribute('style','width: 460px');
				if ($field->getValue()== '') {
					$field->setValue(0);
				}
				$form->addValidationMethod('category', 'sql_select_notnull');
				
		
		// add keywords
		$keywordsSQL = 'SELECT name, id FROM '.$db_ebfw_keywords.' WHERE status = 1 ORDER BY name';
		$field = &$form->addSelectField('keywords');
		$field->setLabel($I18N->msg('ebfw_db_keywords'));
			$select = &$field->getSelect();
				$select->setSize(5);
				$select->addOption(' - - - - - ',0);
				$select->addSqlOptions($keywordsSQL);
				$select->setAttribute('style','width: 460px');
				$select->setAttribute('multiple','multiple');
				if ($field->getValue()== '') {
					$field->setValue(0);
				}
				$form->addValidationMethod('keywords', 'sql_multiselect_notnull');		
			
			
		// add text
		$field = &$form->addTextAreaField('text');	
			$field->setLabel($I18N->msg('ebfw_db_text'));
			$field->setAttribute('style','height: 100px');
			$field->setAttribute('class','tinyMCEEditor');
			$form->addValidationMethod('text', 'empty');
			
			
		// add files
		$field = &$form->addMedialistField('files');
			$field->setLabel($I18N->msg('ebfw_db_text')); 
			
			
		// add fieldset
		$form->addFieldset($I18N->msg('ebfw_db_settings'));
		
		
		// status
		$field = &$form->addSelectField('status');
			$field->setLabel($I18N->msg('ebfw_db_status'));
				$select = &$field->getSelect();
				$select->setSize(1);
				$select->addOption('Online',1);
				$select->addOption('Offline',0);
				$select->setAttribute('style','width: 100px');
				if ($field->getValue()== '') {
					$field->setValue(0);
				}
			
			
		// add param
		$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));
		$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));
		if($func == 'edit') {
			$form->addParam('id', $id);
		}


		// add fieldset
		$form->addFieldset($I18N->msg('ebfw_db_save'));


		// form show
		$form->show();
		

	}


?>