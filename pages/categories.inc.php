<?php


	// vars
	$tbl = $db_ebfw_category;
	$searchtbl = $db_ebfw_entry;
	
	
	// set status
	if( $func == 'status' ) {
		if ($id != 0) {
			echo change_not_inuse_status($tbl, $id, $searchtbl, 'category', $toggle, 'single');
		} $func = '';
	}
	
	
	// set delete
	if( $func == 'delete' ) {
		if ($id != 0) {
			echo $id.' to delete';
		}
		$func = '';
	}


	// list output
	if ($func == '')
	{

	
		// add headline
		echo '<div class="rex-addon-output ebfw-headline">';
		echo '<h2 class="rex-hl2">'.$I18N->msg('ebfw_overview_categories').'</h2>';
		echo '</div>';
		
		
		// id, name, status, createdate, updatedate
		$query = 'SELECT id, name, status  FROM '.$tbl.' ORDER BY name ASC';
		$rowsPerPage = 20;
		$listName = $I18N->msg('ebfw_categories');
		$debug = FALSE;
		$list = new rex_list( $query, $rowsPerPage, $listName, $debug );


		// column width
		$list->addTableColumnGroup(array(
			array('width' => 40),
			array('width' => 'auto'),
			array('width' => 70, 'span' => 2)
		));


		// add new column
		$imgHeader = '<a href="'. $list->getUrl(array('func' => 'add')) .'" class="rex-i-element rex-i-metainfo-add"></a>';
		$list->addColumn($imgHeader, '<span class="rex-i-element rex-i-metainfo"></span>', 0, array( '<th class="rex-icon">###VALUE###</th>', '<td class="rex-icon">###VALUE###</td>' ) );
		$list->setColumnParams ( $imgHeader, array('func' => 'edit', 'id' => '###id###') );


		// remove column
		$list->removeColumn('id');


		// colomn label
		$list->setColumnLabel('name', $I18N->msg('ebfw_categories'));
		$list->setColumnLabel('status', $I18N->msg('ebfw_db_status'));


		// add status function
		$list->setColumnParams('status', array('id' => '###id###', 'start' => $start));
		$list->setColumnFormat('status', 'custom',
			create_function(
				'$params', '$list = $params["list"];
				if($list->getValue("status") == "1") {
					$params = array("func" => "status", "id" => "###id###", "toggle" => "0");
				} else {
					$params = array("func" => "status", "id" => "###id###", "toggle" => "1");
				}
				return $list->getColumnLink("status", $list->getValue("status") != "1" ? "<span style=\'color: red;\'>Offline</span>" : "<span style=\'color: green;\'>Online</span>", $params);'
			)
		);


		// add delete
		$name = ( $list->getRows() > 0 ) ? $list->getValue('name') : 'Entry';
		$list->addColumn('delete',$I18N->msg('delete'),-1,array('<th>'.$I18N->msg('ebfw_db_function').'</th>','<td>###VALUE###</td>'));
		$list->setColumnParams('delete', array('func' => 'delete', 'id' => '###id###'));
		$list->addLinkAttribute('delete','onclick',"return confirm('".$I18N->msg('ebfw_delete_confirm', $name)."');");


		// list show
		$list->show();

	}

	// form output
	elseif ($func == 'edit' || $func == 'add')
	{
		

		// id, name, status, createdate, updatedate
		$tableName = $tbl;
		$fieldset = $I18N->msg('ebfw_edit_categories');
		$whereCondition = 'id='.$id;
		$method = 'post';
		$debug = false;
        $form = new custom_rex_form( $tableName, $fieldset, $whereCondition, $method, $debug, 'custom_rex_form' );


		// add name
		$field = &$form->addTextField('name');
		$field->setLabel($I18N->msg('ebfw_db_name'));
        $form->addValidationMethod('name', 'empty');
		$form->addValidationMethod('name', 'unique');


		// add fieldset
		$form->addFieldset($I18N->msg('ebfw_db_settings'));


		// status
		$field = &$form->addSelectField('status');
		$field->setLabel($I18N->msg('ebfw_db_status'));
		$select = &$field->getSelect();
		$select->setSize(1);
		$select->addOption('Online',1);
		$select->addOption('Offline',0);
		$select->setAttribute('style','width: 100px');
		// Standardwert: 1
		if ($field->getValue()== '') {
			$field->setValue(0);
		}


		// add param
		$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));
		$form->addParam('createdate', rex_formatter :: format(time(), 'strftime', 'date'));
		if($func == 'edit') {
			$form->addParam('id', $id);
		}


		// add fieldset
		$form->addFieldset($I18N->msg('ebfw_db_save'));


		// form show
		$form->show();
		

	}


?>