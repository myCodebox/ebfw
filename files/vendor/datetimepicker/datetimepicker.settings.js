jQuery(function($){
	
	// DEMO: http://xdsoft.net/jqplugins/datetimepicker/
	$('#datetimepicker').datetimepicker({
		inline: true,
		format:'unixtime',
		step: 15
	});
	
});