DROP TABLE IF EXISTS `%TABLE_PREFIX%52623_ebfw_entry`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%52623_ebfw_themes`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%52623_ebfw_category`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%52623_ebfw_keywords`;
DROP TABLE IF EXISTS `%TABLE_PREFIX%52623_ebfw_section`;


/* rex_52623_ebfw_entry */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%52623_ebfw_entry` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	`date` int(11) NOT NULL,
	`themes` varchar(255) NOT NULL default '',
	`category` varchar(255) NOT NULL default '',
	`keywords` varchar(255) NOT NULL default '',
	`text` text NOT NULL default '',	
	`link` varchar(255) NOT NULL default '',
	`files` text NOT NULL default '',
	
	`prio` int(11) NOT NULL,
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`createuser` varchar(255) NOT NULL default '',
	`updatedate` int(11) NOT NULL,
	`updateuser` varchar(255) NOT NULL default '',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* rex_52623_ebfw_themes */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%52623_ebfw_themes` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	`color` varchar(6) NOT NULL default '',
	
	`prio` int(11) NOT NULL,
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`createuser` varchar(255) NOT NULL default '',
	`updatedate` int(11) NOT NULL,
	`updateuser` varchar(255) NOT NULL default '',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* rex_52623_ebfw_category */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%52623_ebfw_category` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	
	`prio` int(11) NOT NULL,
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`createuser` varchar(255) NOT NULL default '',
	`updatedate` int(11) NOT NULL,
	`updateuser` varchar(255) NOT NULL default '',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* rex_52623_ebfw_keywords */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%52623_ebfw_keywords` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	
	`prio` int(11) NOT NULL,
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`createuser` varchar(255) NOT NULL default '',
	`updatedate` int(11) NOT NULL,
	`updateuser` varchar(255) NOT NULL default '',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* rex_52623_ebfw_section */
CREATE TABLE IF NOT EXISTS `%TABLE_PREFIX%52623_ebfw_section` (
	`id` int(11) NOT NULL auto_increment,
	`name` varchar(255) NOT NULL default '',
	`number` varchar(255) NOT NULL default '',
	`info` text NOT NULL default '',
	
	`prio` int(11) NOT NULL,
	`status` int(11) NOT NULL default '0',
	`createdate` int(11) NOT NULL,
	`createuser` varchar(255) NOT NULL default '',
	`updatedate` int(11) NOT NULL,
	`updateuser` varchar(255) NOT NULL default '',
	PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;