<?php


	// addon vars
	$myAddon 			= 'ebfw';
	$myAddonPerm 		= 'ebfw[]';
	$myAddonPermList 	= 'ebfw[list_edit]';
	$myAddonPermSetup 	= 'ebfw[setup]';
	$myAddonName 		= 'Einsatzbericht FW';


	// append lang file
	if ($REX['REDAXO']) {
		// backend
		// use -> $I18N->msg('awkl');
		$I18N->appendFile($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/lang/');
		$myAddonName = $I18N->msg('ebfw');
	}


	// register addon
	$REX['ADDON']['rxid'][$myAddon] = '52623';
	$REX['ADDON']['name'][$myAddon] = $myAddonName;
	$REX['ADDON']['version'][$myAddon] = '0.1';
	$REX['ADDON']['date'][$myAddon] = '19.03.2015 - 1649';
	$REX['ADDON']['update'][$myAddon] = '27.04.2015 - 1619';
	$REX['ADDON']['page'][$myAddon] = $myAddon;
	$REX['ADDON']['author'][$myAddon] = 'Codebox';
	$REX['ADDON']['supportpage'][$myAddon] = 'www.codebox.at';
	$REX['ADDON']['perm'][$myAddon] = $myAddonPerm;


	// permissions
	$REX['PERM'][] = $myAddonPerm;
	$REX['PERM'][] = $myAddonPermList;
	$REX['PERM'][] = $myAddonPermSetup;


	// includes
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/classes/class.custom_rex_form.inc.php');
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/functions/function.ebfw.inc.php');
	require_once($REX['INCLUDE_PATH'] . '/addons/'.$myAddon.'/classes/class.ebfw.inc.php');
	if ($REX['REDAXO']) {
		// backend
		rex_register_extension('PAGE_HEADER', 'ebfw_backend_files');
	} else {
		// frontend
		rex_register_extension('OUTPUT_FILTER', 'ebfw_frontend_files');
	}

	if ($REX['REDAXO']) {
		// subpages
		$subpages = array(
			array('entry', $I18N->msg('ebfw_entry'))			
		);
		// listedit perm
		if (isset($REX['USER']) AND $REX['USER']->isAdmin() AND $REX['USER']->hasPerm($myAddonPermList) ) {
			$list_edit = array(
				array('themes', $I18N->msg('ebfw_themes')),
				array('categories', $I18N->msg('ebfw_categories')),
				array('keywords', $I18N->msg('ebfw_keywords')),
				array('section', $I18N->msg('ebfw_section')),
			);
			$subpages = array_merge($subpages, $list_edit );
		}
		// setup perm
		if (isset($REX['USER']) AND $REX['USER']->isAdmin() AND $REX['USER']->hasPerm($myAddonPermSetup) ) {
			array_push($subpages, array('setup', $I18N->msg('ebfw_setup')) );
		}
		
		$REX['ADDON'][$myAddon]['SUBPAGES'] = $subpages;		
	}


?>