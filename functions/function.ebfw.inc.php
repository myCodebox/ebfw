<?php

	
	// change_status -------------------------------------------------------------------------------
	if(!function_exists('change_status'))
	{
		function change_status($tbl = NULL, $id = 0, $toggle, $debug = FALSE)
		{
			if( !is_null($tbl) OR empty($tbl) ) {
				global $REX;
				$sql = new rex_sql();
				$sql->debugsql = $debug;
				$sql->setTable($tbl);
				$sql->setValue('status', $toggle);
				$sql->setWhere('id = '.intval($id));
				if($sql->update()) {			
					$new_status = ($toggle == 0) ? 'Offline' : 'Online';
					return rex_info('Status wurde f&uuml;r id:' . $id . ' aktualisiert. - ' . $new_status);				
				} else {
					return rex_warning('Status kann nicht ge&auml;ndert werden.');
				}
			}
			return FALSE;
		}
	}
	// change_status -------------------------------------------------------------------------------
	
	
	
	
	// change_not_inuse_status ---------------------------------------------------------------------
	if(!function_exists('change_not_inuse_status'))
	{
		function change_not_inuse_status($tbl = NULL, $id = 0, $searchtbl = NULL, $searchfield = 'id', $toggle, $what = 'single', $debug = FALSE)
		{
			global $REX;
			
			if( (!is_null($tbl) OR empty($tbl)) AND (!is_null($searchtbl) OR empty($searchtbl)) ) {
				switch($what){					
					// --------------------
					case 'multi': 	$searchqry = $searchfield.' LIKE ("%|'.$id.'|%")';
						break;
					// --------------------
					case 'singel':
					default: 		$searchqry = $searchfield.' LIKE ("%'.$id.'%")';
						break;
					// --------------------						
				}
				
				// is id in use
				$useqry = 'SELECT id FROM '.$searchtbl.' WHERE '.$searchqry.' AND status = 1 ORDER BY id';				
				$sql = rex_sql::factory();
				$sql->debugsql = $debug;
				$sql->setQuery($useqry);
				$rows = $sql->getRows();
				
				if($rows > 0 AND $toggle == 0) {
					return rex_warning('Der Status kann nicht ge&auml;ndert werden. Dieser Eintrag ist "'.$rows.'" mal in verwendung.');
				} else {
					$sql = new rex_sql();
					$sql->debugsql = $debug;
					$sql->setTable($tbl);
					$sql->setValue('status', $toggle);
					$sql->setWhere('id = '.intval($id));
					if($sql->update()){
						$new_status = ($toggle == 0) ? 'Offline' : 'Online';
						return rex_info('Status wurde f&uuml;r id:' . $id . ' aktualisiert. - ' . $new_status);	
					} else {
						return rex_warning('Status kann nicht ge&auml;ndert werden.');
					}
				}
				
			}
			return FALSE;
		}
	}
	// change_not_inuse_status ---------------------------------------------------------------------
	
	
	
	
	// getIdToText ---------------------------------------------------------------------------------
	/*
	 * DBs
	 * rex_1112_kl_categories
	 * rex_1112_kl_themes
	 * rex_1112_kl_keywords
	 * 
	 */
	if( !function_exists('getIdToText') ) {
		function getIdToText($id = 0, $dbname = NULL, $fields = 'name', $online = TRUE, $debug = FALSE) {
			if( !is_null($dbname) ) {
				$on = ( $online ) ? 'status = 1 AND' : '';
				$qry = 'SELECT '.$fields.' FROM '.$dbname.' WHERE '.$on.' id = '.$id.' LIMIT 1';
				$sql = new rex_sql();
				$sql->debugsql = $debug;
				$sql->setQuery($qry);
				$arr = $sql->getArray();
				if( count($arr) > 0 ) {
					foreach( $arr AS $key => $val ) {
						return $val['name'];
					}
				}
			}
			return FALSE;
		}
	}
	// getIdToText ---------------------------------------------------------------------------------
	
	
	
	
	// ebfw_backend_files --------------------------------------------------------------------------
	if (!function_exists('ebfw_backend_files')) {
		function ebfw_backend_files($params) {
			global $REX;
			$page = rex_request('page', 'string');
			$subpage = rex_request('subpage', 'string');
			if( $page == 'ebfw' ) {
				echo '
					<!-- colpick
					<link type="text/css" rel="stylesheet" href="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/vendor/colpick/css/colpick.css" />
					<script type="text/javascript" src="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/vendor/colpick/js/colpick.js"></script>
					-->
					
					<!-- datetimepicker -->
					<link type="text/css" rel="stylesheet" href="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/vendor/datetimepicker/jquery.datetimepicker.css" />
					<script type="text/javascript" src="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/vendor/datetimepicker/jquery.datetimepicker.js"></script>
					<script type="text/javascript" src="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/vendor/datetimepicker/datetimepicker.settings.js"></script>
					
					<!-- backend design -->
					<link type="text/css" rel="stylesheet" href="../'.$REX['MEDIA_ADDON_DIR'].'/ebfw/ebfw_design_backend.css" />
				';
			}
		}
	}
	// ebfw_backend_files --------------------------------------------------------------------------
	
	
	
	
	// ebfw_frontend_files -------------------------------------------------------------------------
	if (!function_exists('ebfw_frontend_files')) {
		function ebfw_frontend_files($params) {
			global $REX;
			$insert = '';
			$insert .= '<!-- frontend design -->';
			$insert .= '<link type="text/css" rel="stylesheet" href="'.$REX['MEDIA_ADDON_DIR'].'/ebfw_/ebfw_design.css" />';
			$insert .= '<script type="text/javascript" src="'.$REX['MEDIA_ADDON_DIR'].'/ebfw/ebfw_init.js"></script>';
			$search = '</head>';
			$replace = $insert.'</head>';
			return str_replace($search, $replace, $params['subject']);			
		}
	}
	// ebfw_frontend_files -------------------------------------------------------------------------
	
	
	
	
	// getModuleFiles ------------------------------------------------------------------------------
	if (!function_exists('readDirFiles')) {
		function readDirFiles($path = NULL) {
			$arr = '';
			if( !is_null($path) ) {
				if ($handle = opendir($path)) {
					while (false !== ($entry = readdir($handle))) {		
						if ($entry != "." AND $entry != "..") {
							$arr[] = $entry;
						}
					}		
					closedir($handle);
				}
				return $arr;
			}
			return FALSE;
		}
	}
	// getModuleFiles ------------------------------------------------------------------------------
	
	
	
	
	// getModuleFiles ------------------------------------------------------------------------------
	if (!function_exists('getModuleFiles')) {
		function getModuleFiles($fileArr = NULL) {
			$arr = '';
			if( !is_null($fileArr) AND is_array($fileArr) ) {
				foreach( $fileArr AS $key => $var ) {
					$entryArr = explode('.',$var);
					if( is_array($entryArr) AND count($entryArr) == 3 ) {						
						list($m_name,$m_what,$m_ending) = $entryArr;
						if( $m_ending == 'inc' AND ($m_what == 'in' OR $m_what == 'out') ) {
							$arr[$m_name]['searchtext'] = '// module:'.$m_name;
							$arr[$m_name][$m_what] = $var;
						}
					}
				}
				return $arr;
			}
			return FALSE;
		}
	}
	// getModuleFiles ------------------------------------------------------------------------------
	
	
	
	
	// print_pre -----------------------------------------------------------------------------------
	if( !function_exists('print_pre') ) {
		function print_pre($val, $subclass = '', $style = NULL){
			echo '<pre class="print_pre '.$subclass.'" style="'.$style.'">';
			print_r($val);
			echo  '</pre>';
		}
	}
	// print_pre -----------------------------------------------------------------------------------
	
	
	
	
	// in_array_multi ------------------------------------------------------------------------------
	if( !function_exists('in_array_multi') ) {
		function in_array_multi($needle, $haystack, $strict = false) {
			foreach ($haystack as $item) {
				if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_multi($needle, $item, $strict))) {
					return true;
				}
			}
			return false;
		}
	}
	// in_array_multi ------------------------------------------------------------------------------
	
	
	
	
	// sort_array_multi ----------------------------------------------------------------------------
	// http://stackoverflow.com/questions/777597/sorting-an-associative-array-in-php
	if( !function_exists('sort_array_multi') ) {
		function sort_array_multi(&$array, $subkey, $sortType = SORT_ASC) {
			foreach ($array as $subarray) {
				$keys[] = $subarray[$subkey];
				#echo $subarray['file_lang'].'<br />';
			}
			array_multisort($keys, $sortType, $array);
		}		
	}
	// sort_array_multi ----------------------------------------------------------------------------
	
	
	
	
	// rip_tags ------------------------------------------------------------------------------------
	// http://php.net/manual/de/function.strip-tags.php#110280
	if (!function_exists('rip_tags')) {
		function rip_tags($string) {   
			// ----- remove HTML TAGs -----
			$string = preg_replace ('/<[^>]*>/', ' ', $string);
		   
			// ----- remove control characters -----
			$string = str_replace("\r", '', $string);    // --- replace with empty space
			$string = str_replace("\n", ' ', $string);   // --- replace with space
			$string = str_replace("\t", ' ', $string);   // --- replace with space
		   
			// ----- remove multiple spaces -----
			$string = trim(preg_replace('/ {2,}/', ' ', $string));
		   
			return $string;		
		}
	}
	// rip_tags ------------------------------------------------------------------------------------
	
	
	
	
	// shortText -----------------------------------------------------------------------------------
	// http://www.hoerandl.com/code-schnipsel/php-codes/string-funktionen/item/einen-lange-text-auf-eine-bestimmte-laenge-kuerzen
	if (!function_exists('shortText')) {
		function shortText($string, $lenght) {
			$string = rip_tags($string);
			if(strlen($string) > $lenght) {
				$string = substr($string,0,$lenght)."...";
				$string_ende = strrchr($string, " ");
				$string = str_replace($string_ende," ...", $string);
			}
			return $string;
		}
	}
	// shortText -----------------------------------------------------------------------------------
	
	
	
	
	// addhttp -------------------------------------------------------------------------------------
	if( !function_exists('addhttp') ) {
		function addhttp($url) {
			if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
				$url = "http://" . $url;
			}
			return $url;
		}
	}
	// addhttp -------------------------------------------------------------------------------------
	
	
	
	
?>
