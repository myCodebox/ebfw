<?php




/*
 * Source: http://www.redaxo.org/de/forum/addons-f30/wie-in-rex-form-pflichtfeld-erstellen-t13749.html
 *
 * Label: my_label zB.: name
 * Type: 'empty', 'number', 'email', 'unique'
 * Fieldset: NULL / $this->getFieldsetName()
 * 
 */




class custom_rex_form extends rex_form {





	protected $validationElements;
	protected $validationError;
	protected $validationErrorMsg;
	

	
	
	#public function delete()
	#{
	#	return 'Kann nicht gelöscht werden!';
	#	exit;
	#}
	#
	#public function save()
	#{
	#	return 'Kann nicht gespeichert werden!';
	#	exit;
	#}
	
	
	
	public function validate()
	{
		if(empty($this->validationElements)) return true;
		foreach ($this->validationElements AS $type) {
			
			

			$fieldValue = $this->elementPostValue($type['fieldset'], $type['label']);
			$fieldLabel = $this->getElement($type['fieldset'], $type['label']);			
			$fieldAttr  = $fieldLabel->getAttribute('class');
			
			$this->validationError = $this->_validate($type['type'], $type['label'], $fieldValue, $fieldLabel);
			if (isset($this->validationError)) {
				$this->validationErrorMsg[] = $this->validationError;
				$fieldLabel->setAttribute('class', $fieldAttr.' crf-error');
				$fieldLabel->setAttribute('style','border: 1px solid #e34; padding: 2px; background-color: #FAE9E5;');
			}
		}
		if( count($this->validationErrorMsg) > 0 ) {
			$msg = '';
			foreach($this->validationErrorMsg AS $val ) {
				$msg .= $val.'<br />';
			}
			return $msg;
		}
		return parent::validate();
	}
	
	
	
	/*
	 * Label: my_label zB.: name
	 * Type: 'empty', 'number', 'email', 'sql_select_notnull', 'sql_multiselect_notnull', 'unique'
	 * Fieldset: NULL / $this->getFieldsetName()
	 */
	public function addValidationMethod($label, $type, $fieldset = NULL)
	{
		if( $fieldset == NULL ) {
			$fieldset = $this->getFieldsetName();
		}
		$this->validationElements[] = array(
			'label' 	=> $label,
			'type' 		=> $type,
			'fieldset' 	=> $fieldset
		);
	}
	
	
	
	
	private function _validate($type, $label, $fieldValue, $fieldLabel)
	{
		
		global $REX, $I18N;
		
		switch ($type) {
			// status -------------------------------------------------------------------------------
			case 'isinuse':
				if ($fieldValue == 1) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" darf nicht geÃ¤nder werden.';
					return $I18N->msg('crf_error_isinuse', $fieldLabel->label);
				}
			break;
		
			// empty -------------------------------------------------------------------------------
			case 'empty':
				if (empty($fieldValue)) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" darf nicht leer sein.';
					return $I18N->msg('crf_error_empty', $fieldLabel->label);
				}
			break;

			// number ------------------------------------------------------------------------------
			case 'number':
				if (!is_numeric($fieldValue)) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" muss eine Zahl sein.';
					return $I18N->msg('crf_error_number', $fieldLabel->label);
				}
			break;

			// email -------------------------------------------------------------------------------
			case 'email':
				if(!filter_var($fieldValue, FILTER_VALIDATE_EMAIL)) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" ist keine E-Mail-Adresse.';
					return $I18N->msg('crf_error_email', $fieldLabel->label);
				}
			break;
		
			// select_notnull-----------------------------------------------------------------------
			case 'sql_select_notnull':
				if($fieldValue <= 0) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" darf nicht leer sein.';
					return $I18N->msg('crf_error_sql_select_notnull', $fieldLabel->label);
				}
			break;
		
			// multiselect_notnull -----------------------------------------------------------------
			case 'sql_multiselect_notnull':
				$error = FALSE;
				if( !isset($fieldValue) ) {
					$error = TRUE;				
				} else {
					if(in_array(0, $fieldValue)) {
						$error = TRUE;	
					}
				}
				if( $error ) {
					#return 'Das Eingabefeld "' . $fieldLabel->label . '" darf nicht leer sein.';
					return $I18N->msg('crf_error_sql_multiselect_notnull', $fieldLabel->label);
				}
			break;

			// unique ------------------------------------------------------------------------------
			case 'unique':
				$notMyId = '';
				if( $this->isEditMode() ) { $notMyId = 'AND id !='.$this->getParam('id'); }

				$qry = 'SELECT '.$label.' FROM '.$this->tableName.' WHERE '.$label.'="'.$fieldValue.'" '.$notMyId.' LIMIT 1';
				$sql = new rex_sql();
				$sql->debugsql = FALSE;				
				if(count($sql->getArray($qry)) > 0) {
					#return 'Der Eintrag "' . $fieldValue . '" ist schon vorhanden.';
					return $I18N->msg('crf_error_sql_multiselect_notnull', $fieldValue);
				}
			break;
		}
	}
	
	
	
	
}
