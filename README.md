# README #

EBFW [Einsatzbericht FW] ist ein Redaxo Addon

### Was ist EBFW ###

* Redaxo Addon
* Version 0.1

### Setup ###

* Add this repo to the Redaxo addon folder
* Change the name of the folder to "ebfw"
* Install it on the Redaxo backend page

### Permission ###

* Set the right permission  in the user area
* ebfw[list_edit] -> Theme, Cat., Key. and Section edit
* ebfw[setup] -> install the Module


### TODO ###

* Script Testing:
	* http://xdsoft.net/jqplugins/datetimepicker/
	* http://aef-.github.io/jquery.filthypillow/


### Developer ###

* https://www.codebox.at