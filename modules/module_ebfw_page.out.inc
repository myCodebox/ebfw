<?php

	# Hier nichts ändern
	// module:module_ebfw_page
	# Hier nichts ändern

	if ( OOAddon::isAvailable('ebfw') != 1 ) {
		echo 'Bitte installieren Sie das "EBFW" Addon';
	} else {
		if( !$REX['REDAXO'] ) {
			// FRONTEND
			include($REX["INCLUDE_PATH"].'/addons/ebfw/outputs/ebfw_page.out.inc.php');
		} else {
			// BACKEND
			echo 'Ausgabe vom "EBFW" Addon!';
		}
	}

?>